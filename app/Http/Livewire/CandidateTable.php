<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Candidate;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\DateColumn;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;
use Mediconesystems\LivewireDatatables\NumberColumn;

class CandidateTable extends LivewireDatatable
{
    public $model = Candidate::class;

    public function columns()
    {
        return[
            NumberColumn::name('candidate_id')->label('Id')->sortBy('candidate_id'),
            Column::name('full_name')->label('Fullname'),
            Column::name('dob')->label('Date of Birth'),
            Column::name('pob')->label('Place of Birth'),
            Column::name('gender')->label('Gender'),
            Column::name('year_exp')->label('Year Experience'),
            Column::name('last_salary')->label('Last Salary'),
            DateColumn::name('created_at')
                ->label('Created at')
        ];
    }
}