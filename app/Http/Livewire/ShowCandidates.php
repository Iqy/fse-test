<?php

namespace App\Http\Livewire;

use App\Models\Candidate;
use Livewire\Component;

class ShowCandidates extends Component
{
    public $full_name, $dob, $pob, $gender, $year_exp, $last_salary;

    protected $rules = [
        'full_name' => 'required',
        'dob' => 'required',
        'pob' => 'required',
        'gender' => 'required',
        'year_exp' => 'required',
        'last_salary' => 'required',
    ];

    public function render()
    {
        return view('livewire.show-candidates');
    }
    public function save(){
        $this->validate();

        $data = Candidate::create([
            'full_name' => $this->full_name,
            'dob' => $this->dob,
            'pob' => $this->pob,
            'gender' => $this->gender,
            'year_exp' => $this->year_exp,
            'last_salary' => $this->last_salary,
        ]);
    }
}
