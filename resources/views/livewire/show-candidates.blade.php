<div>
    <div class="row" wire:ignore>
        <div class="col-12">
            <h1 class="text-center">Data Candidate</h1>
        </div>
        <div class="col-12">
            <table class="table table-striped" id="dataTable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Fullname</th>
                        <th>Date Born</th>
                        <th>Place Born</th>
                        <th>Gender</th>
                        <th>Experience</th>
                        <th>Last Salary</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <form wire:submit.prevent='save'>
        <div class="row">
            <div class="col-12">
                <h1 class="text-center mt-3">Insert Candidate</h1>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <div>
                        <label for="exampleInputEmail1" class="form-label">Fullname</label>
                        <input name="full_name" type="text" class="form-control" id="full_name" aria-describedby="emailHelp" placeholder="Ex: John Doe" wire:model='full_name'>
                        @error('fullname') <span class="error text-danger">{{ $message }}</span> @enderror
                    </div>
                    <div class="mt-1">
                        <div class="d-flex">
                            <div class="form-check">
                            <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault1" wire:model='gender' value="M">
                            <label class="form-check-label" for="flexRadioDefault1">
                                Male
                            </label>
                        </div>
                        <div class="form-check ms-3">
                            <input class="form-check-input" type="radio" name="gender" id="flexRadioDefault2" wire:model='gender' value="F">
                            <label class="form-check-label" for="flexRadioDefault2">
                                Female
                            </label>
                        </div>
                    </div>
                    @error('gender') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>
            </div>

            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Date of Born</label>
                    <input type="date" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="dob" wire:model='dob'>
                    @error('dob') <span class="error text-danger">{{ $message }}</span> @enderror
                </div>
            </div>
            <div class="col-6">
                <div class="mb-3">
                    <label for="pob" class="form-label">Place of Born</label>
                    <input type="text" class="form-control" id="pob" aria-describedby="emailHelp" placeholder="Example: Place name" name="pob" wire:model='pob'>
                    @error('pob') <span class="error text-danger">{{ $message }}</span> @enderror

                </div>
            </div>
            <div class="col-6">
                <div class="row">
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="year_exp" class="form-label">Year of Experienced</label>
                            <input type="number" class="form-control" id="year_exp" aria-describedby="emailHelp" placeholder="Example: 3" name="year_exp" wire:model='year_exp'>
                            @error('year_exp') <span class="error text-danger">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mb-3">
                            <label for="last_salary" class="form-label">Last Salary</label>
                            <input type="number" class="form-control" id="last_salary" aria-describedby="emailHelp" placeholder="Example: 2500000" name="last_salary" wire:model='last_salary'>
                            @error('last_salary') <span class="error text-danger">{{ $message }}</span> @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary w-100">
                <div wire:loading>
                    <div class="spinner-border spinner-border-sm" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
                {{-- <div class="spinner-border spinner-border-sm" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div> --}}
                Submit
            </button>
        </div>
    </form>
</div>
