<x-layouts.layouts>
    <x-slot name='style'></x-slot>

    <div class="container my-5">
        {{-- <livewire:candidate-table></livewire:candidate-table> --}}
        <livewire:show-candidates></livewire:show-candidates>
    </div>

    <script>

        $(document).ready(function(){
            $("#dataTable").DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('candidate.show') }}',
                columns: [
                    { data: 'candidate_id', name: 'candidate_id' },
                    { data: 'full_name', name: 'full_name' },
                    { data: 'dob', name: 'dob' },
                    { data: 'pob', name: 'pob' },
                    { data: 'gender', name: 'gender' },
                    { data: 'year_exp', name: 'year_exp' },
                    { data: 'last_salary', name: 'last_salary' },
                ],
            })
        })
    </script>
    <x-slot name='script'>
        
    </x-slot>
</x-layouts.layouts>