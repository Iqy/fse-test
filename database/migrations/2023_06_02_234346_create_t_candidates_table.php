<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_candidates', function (Blueprint $table) {
            $table->bigIncrements('candidate_id');
            $table->string('full_name', 50);
            $table->string('dob', 10);
            $table->string('pob', 50);
            $table->string('gender', 2);
            $table->string('year_exp', 10);
            $table->string('last_salary', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_candidates');
    }
}
