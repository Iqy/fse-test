<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CandidateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $gender = $this->faker->randomElement(['M', 'F']);
        return [
            'full_name' => $this->faker->name($gender),
            'dob' => $this->faker->date('Y-m-d', '2000-01-31'),
            'pob' => $this->faker->city(),
            'gender' => $gender,
            'year_exp' => $this->faker->randomDigit(),
            'last_salary' => $this->faker->randomElement([
                '2000000',
                '3000000',
                '4000000',
                '5000000',
                '6000000',
                '7000000',
                '8000000',
                '9000000',
                '10000000',
            ])
        ];
    }
}
